//
//  PlayerViewController.swift
//  SyncPlayer
//
//  Created by Lyes HAMIDOUCHE on 26/02/2019.
//  Copyright © 2019 Lyes HAMIDOUCHE. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class PlayerViewController: AVPlayerViewController{
    var videoName: String = "Video.mp4"
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = "Player"
        loadVideo(videoName)
        //player?.play()
        // Do any additional setup after loading the view.
    }
    

    func loadVideo (_ name: String) {
        player = AVPlayer()
        var pathUrl: URL {
            return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent(name)
        }
        NSLog("\(pathUrl)===>to load")
        player?.replaceCurrentItem(with: AVPlayerItem(url: pathUrl))
        player?.addObserver(self, forKeyPath: "status", options: NSKeyValueObservingOptions()   , context: nil)
        showsPlaybackControls = false
        self.navigationController?.setNavigationBarHidden(true, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if player?.status == AVPlayer.Status.readyToPlay {
            player?.pause()
        }
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification){
        print("Video Finished")
    }
    
    func play() {
        self.player?.play()
    }
    func pause() {
        self.player?.pause()
        //let apDel = UIApplication.shared.delegate as! AppDelegate
        NSLog("Time:\(self.player?.currentItem?.currentTime().seconds)\r\n")
       
    }
    func pause(timestamp: Double) {
        self.player?.pause()
        self.player?.seek(to: CMTime(value:CMTimeValue(timestamp),timescale:1))
        self.player?.pause()
        
    }
    
    
    func stop() {
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        self.player?.pause()
        self.player?.seek(to: CMTimeMake(value: 0,timescale: 1))
        self.player?.pause()
        
    }
}
