//
//  ConnectionViewController.swift
//  SyncPlayer
//
//  Created by Lyes HAMIDOUCHE on 23/02/2019.
//  Copyright © 2019 Lyes HAMIDOUCHE. All rights reserved.
//

import UIKit
import  CocoaAsyncSocket

class ConnectionViewController: UIViewController {
    var availableServers: NSMutableArray?
    
    @IBOutlet weak var connectButton: UIButton!
    @IBOutlet weak var serverSelect: UISegmentedControl!
    @IBOutlet weak var serverAddress: UITextField!
    @IBOutlet weak var remoteSwitch: UISwitch!
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil,bundle: nibBundleOrNil)
        
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    
    @objc func didPressConnectButton(sender: UIButton?) {
        NSLog("Button '%@' has been pressed ... triggering connection", sender?.titleLabel?.text ?? "none")
        // Add code for connection
        let appDel = UIApplication.shared.delegate as! AppDelegate
        // Create vc
        let storyboard  = UIStoryboard(name:"Main",bundle:nil)
        if remoteSwitch.isOn {
            
            let remoteVC : RemoteViewController = storyboard.instantiateViewController(withIdentifier: "RemoteViewController") as! RemoteViewController
            appDel.remoteViewController = remoteVC
                //Create the future view controller
            self.navigationController?.pushViewController( appDel.remoteViewController!, animated: true)
            
            
        } else {
            let settingsVC : SettingsViewController = storyboard.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            NSLog("Log %d",self.navigationController == nil)
            appDel.settingViewController = settingsVC
            //Create the future view controller
            self.navigationController?.pushViewController( appDel.settingViewController!, animated: true)
        }
        
        var address : String? = serverSelect.titleForSegment(at: 0)
        if serverSelect.selectedSegmentIndex == 1 {
            address = serverAddress.text
        }
        appDel.address = address!
        appDel.port = 5555
        appDel.socket = GCDAsyncSocket(delegate: appDel, delegateQueue: DispatchQueue.main)
        do {
            try appDel.socket?.connect(toHost: address!, onPort: 5555)
        } catch {
            NSLog ("Error \(error)")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.connectButton?.addTarget(self, action: #selector(didPressConnectButton) , for: .touchDown)
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.title = "Connection/Server Information"
        if serverSelect.selectedSegmentIndex == 0 {
            serverAddress.isEnabled = false
        } else {
            serverAddress.isEnabled = true
        }
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func serverSegmentChanged(_ sender: Any) {
        NSLog("Value changed to %d",serverSelect.selectedSegmentIndex)
        if serverSelect.selectedSegmentIndex == 0 {
            serverAddress.isEnabled = false
        } else {
            serverAddress.isEnabled = true
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
