//
//  ViewController.swift
//  SyncPlayer
//
//  Created by Lyes HAMIDOUCHE on 22/02/2019.
//  Copyright © 2019 Lyes HAMIDOUCHE. All rights reserved.
//

import UIKit
import AVKit
class SettingsViewController: UIViewController, UIPickerViewDataSource,UIPickerViewDelegate, UITableViewDataSource, UITableViewDelegate, DownloadInfoDelegate, AVPlayerViewControllerDelegate{
    
    
    
    @IBOutlet weak var pickerview: UIPickerView!
    @IBOutlet weak var downloadsButton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var connectStatus: UILabel!
    
    
    
    var iPadsCount = 30
    var selectedPosition: Int = 1
    var downloads = NSMutableArray()
    var downloadVC:DownloadTableViewController?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        NSLog("Settings View Controller has been loaded")
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        self.navigationItem.title = "Settings"
        self.pickerview.delegate = self
        self.pickerview.dataSource = self
        self.pickerview.setNeedsLayout()
        self.tableView.dataSource = self
        self.tableView.delegate = self
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return iPadsCount
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        NSLog("did select row %d from component",row,component)
        selectedPosition = row+1
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
    
        return String(format: "iPad n° %d", row+1)
    }
    
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return downloads.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //NSLog("Table cell update")
        let cell = tableView .dequeueReusableCell(withIdentifier: "ddlCell") as! VideoDownloadTableViewCell
        let dInfo: DownloadInfo = downloads.object(at: indexPath.row) as! DownloadInfo
        cell.filenameLabel.text = dInfo.title
        let i:Int = Int(dInfo.percentage*100.0)
        cell.percentageLabel.text = String(format: "\(i)%%")
        cell.progressBar.progress = Float(dInfo.percentage)
        cell.setNeedsLayout()
        return cell
    }
    
    
    
    func progressUpdate(_ dInfo: DownloadInfo) {
        NSLog("Progress update")
        DispatchQueue.main.async {
            self.tableView.reloadData()
            self.tableView.setNeedsDisplay()
        }
    }
    
    
    @IBAction func downloadVideos(_ sender: Any) {
        
        let apDel = UIApplication.shared.delegate as! AppDelegate
        for ddl in downloads {
            let d :DownloadInfo = ddl as! DownloadInfo
            d.downloadTask?.cancel()
            
        }
        downloads.removeAllObjects()
        tableView.setNeedsDisplay()
        
        if apDel.socket != nil {
            apDel.socket?.write(Data("GetVideos\r\n".utf8), withTimeout: -1, tag: 0)
        }
        
        

//        let storyborad = UIStoryboard.init(name: "Main", bundle: nil)
//        let playerVC : PlayerViewController =  storyborad.instantiateViewController(withIdentifier:"PlayerViewController") as! PlayerViewController
//        playerVC.delegate = self
//
//        self.navigationController?.pushViewController(playerVC, animated: true)
        
    }
    
    func playerViewController(_ playerViewController: AVPlayerViewController, failedToStartPictureInPictureWithError error: Error) {
        NSLog("Error %s",error.localizedDescription)
    }
    
    
    
    
    
}

