//
//  RemoteViewController.swift
//  SyncPlayer
//
//  Created by Lyes HAMIDOUCHE on 07/03/2019.
//  Copyright © 2019 Lyes HAMIDOUCHE. All rights reserved.
//

import UIKit

class RemoteViewController: UIViewController,UITableViewDelegate,UITableViewDataSource{
    
    @IBOutlet weak var VideoList: UITableView!
    @IBOutlet weak var connectStatus: UILabel!
    var videoList: NSMutableArray = NSMutableArray()
    let apDel :AppDelegate = UIApplication.shared.delegate as! AppDelegate
    var currentRow: Int = 0
    var totalElapsedTime: UInt64 = 0
    var startDate: DispatchTime? = nil
    override func viewDidLoad() {
        super.viewDidLoad()
        self.VideoList.delegate = self
        self.VideoList.dataSource = self
        if apDel.connected && apDel.socket != nil {
            apDel.socket?.write(Data("VideoList\r\n".utf8), withTimeout: -1, tag: 0)
        }
    }
    

    @IBAction func prepare(_ sender: Any) {
        if apDel.socket != nil {
            apDel.socket?.write(Data("Prepare:\(videoList[currentRow] as! String):\r\n".utf8), withTimeout: -1, tag: 0)
            totalElapsedTime = 0
        }
    }
    @IBAction func play(_ sender: Any) {
        apDel.socket?.write(Data("Play:\r\n".utf8), withTimeout: -1, tag: 0)
        startDate = DispatchTime.now()
    
    }
    @IBAction func pause(_ sender: Any) {
        let currentTime = DispatchTime.now()
        let nanoElapsedTime = currentTime.uptimeNanoseconds - (startDate?.uptimeNanoseconds)!
        startDate = nil
        self.totalElapsedTime = self.totalElapsedTime + (nanoElapsedTime/1000000) // Convert nano to milliseconds
        let d:Double  = Double(self.totalElapsedTime)/1000.0
        apDel.socket?.write(Data("Pause:\(d):\r\n".utf8), withTimeout: -1, tag: 0)
    }
    @IBAction func stop(_ sender: Any) {
        apDel.socket?.write(Data("Stop:\r\n".utf8), withTimeout: -1, tag: 0)
        totalElapsedTime = 0
    
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        NSLog("\(videoList.count) ==> videoList.count")
        return videoList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell:UITableViewCell = tableView.dequeueReusableCell(withIdentifier:"title", for: indexPath)

        cell.textLabel?.text =  (videoList.object(at: indexPath.row) as! String)
        cell.setNeedsDisplay()
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        currentRow = indexPath.row
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
