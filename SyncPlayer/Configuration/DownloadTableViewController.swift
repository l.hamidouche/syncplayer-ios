//
//  DownloadTableViewController.swift
//  SyncPlayer
//
//  Created by Lyes HAMIDOUCHE on 25/02/2019.
//  Copyright © 2019 Lyes HAMIDOUCHE. All rights reserved.
//

import UIKit

class DownloadTableViewController: UITableViewController {
    
    var downloads = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        NSLog("view did load")
        self.tableView.register(VideoDownloadTableViewCell.self, forCellReuseIdentifier: "ddlCell")
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        self.tableView.register(VideoDownloadTableViewCell.self, forCellReuseIdentifier: "ddlCell")
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return downloads.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier:"ddlCell", for: indexPath)
        //NSLog("%s",cell.reuseIdentifier)
        let dInfo = downloads.object(at:indexPath.row) as! DownloadInfo
        //downloadInfo: DownloadInfo  = downloads.object(at: indexPath.row)
        
        
        return cell
        
    }
    

   
    

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
}
