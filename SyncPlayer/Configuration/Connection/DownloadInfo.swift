//
//  DownloadInfo.swift
//  SyncPlayer
//
//  Created by Lyes HAMIDOUCHE on 25/02/2019.
//  Copyright © 2019 Lyes HAMIDOUCHE. All rights reserved.
//

import Foundation
import AFNetworking

class DownloadInfo {
    
    var title: String
    var percentage: Double
    var link: String?
    var delegate: DownloadInfoDelegate?
    var downloadTask : URLSessionDownloadTask?
    init () {
        self.title = "Unknown"
        self.percentage = 0
        self.link = "http://somewhere.com/something.mp4"
        self.delegate = nil
        self.downloadTask = nil
    }
    
    init (_ link: String, percentage: Double) {
        let parts = link.components(separatedBy: "/")
        self.title = parts.last!
        self.link = link
        self.percentage = percentage
        self.delegate = nil
        self.downloadTask = nil
    }
    
    
    func startDownload(_ delegate:DownloadInfoDelegate) {
        if self.link == nil || self.title == "Unknown" {
            return
        }
        self.delegate = delegate
        var pathUrl: URL {
            return try! FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent(self.title)
        }
        print(pathUrl.path)
        let request : NSURLRequest = NSURLRequest(url: URL(string: self.link!)!)
        let session  = AFHTTPSessionManager()
        //var progress : Progress = Progress()
        //
        let fileManager : FileManager = FileManager.default
        
        do {
            try fileManager.removeItem(at: pathUrl)
        } catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
        let obj = self
        self.downloadTask  = session.downloadTask(with: request as URLRequest, progress: { (p:Progress) in
            obj.percentage = p.fractionCompleted
            let d: Int = Int(p.fractionCompleted * 100.0)
            DispatchQueue.main.async {
               
                obj.delegate!.progressUpdate(obj)
                
            }
            
        }, destination: { (URL, URLResponse) -> URL in
            return pathUrl
        }) { (r :URLResponse, u: URL?, e:Error?) in
           
            if obj.percentage == 1 {
                NSLog("DownloadCompleted with success")
                DispatchQueue.main.async {
                    let c:UIAlertController = UIAlertController.init(title: "Information", message: "Download of \(obj.title) has finished successfully", preferredStyle: UIAlertController.Style.alert)
                    let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        print("You've pressed default");
                    }
                    c.addAction(action1)
                    let apdel = UIApplication.shared.delegate as! AppDelegate
                    apdel.settingViewController!.present(c,animated: true,completion: nil)
                }
                
                print("\(pathUrl.path) exists ? ---> \(fileManager.fileExists(atPath: pathUrl.path))")
            } else {
                
               NSLog("Download failed")
                DispatchQueue.main.async {
                    let c:UIAlertController = UIAlertController.init(title: "Information", message: "Download of \(obj.title) has failed", preferredStyle: UIAlertController.Style.alert)
                    let action1 = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction) in
                        print("You've pressed default");
                    }
                    c.addAction(action1)
                    let apdel = UIApplication.shared.delegate as! AppDelegate
                    apdel.settingViewController!.present(c,animated: true,completion: nil)
                    //c.show(apdel.settingViewController!, sender: self)
                    print("\(pathUrl.absoluteString) exists ? ---> \(fileManager.fileExists(atPath: pathUrl.absoluteString))")
                    
                }
                
            }
        }
        
        downloadTask!.resume()
        
    }
    
}


protocol DownloadInfoDelegate {
    func progressUpdate(_ dInfo: DownloadInfo)
}
