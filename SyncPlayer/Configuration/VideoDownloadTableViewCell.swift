//
//  VideoDownloadTableViewCell.swift
//  SyncPlayer
//
//  Created by Lyes HAMIDOUCHE on 25/02/2019.
//  Copyright © 2019 Lyes HAMIDOUCHE. All rights reserved.
//

import UIKit
import AVFoundation
class VideoDownloadTableViewCell: UITableViewCell {
    
    @IBOutlet weak var filenameLabel: UILabel!
    @IBOutlet weak var percentageLabel: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        NSLog("Awake from nib here")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
