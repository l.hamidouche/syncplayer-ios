//
//  AppDelegate.swift
//  SyncPlayer
//
//  Created by Lyes HAMIDOUCHE on 22/02/2019.
//  Copyright © 2019 Lyes HAMIDOUCHE. All rights reserved.
//

import UIKit
import CocoaAsyncSocket
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GCDAsyncSocketDelegate , UINavigationControllerDelegate{
    
    var window: UIWindow?
    var socket: GCDAsyncSocket?
    var navigatinController : UINavigationController?
    var connectViewController: ConnectionViewController?
    var playerViewController: PlayerViewController?
    var settingViewController: SettingsViewController?
    var remoteViewController: RemoteViewController?
    var videoList : NSMutableArray = NSMutableArray()
    var address : String = "127.0.0.1"
    var port : UInt16 = 5555
    let uuid  = UUID().uuidString
    var connected = false
    let maxRetries  = 5
    let retryDelay = Date(timeIntervalSinceNow: 5)
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        print(uuid)
        window = UIWindow(frame: UIScreen.main.bounds)
        NSLog("Window problem: ... not sure")
        if let window = window {
            NSLog("Window problem 2: ... not sure")
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            self.connectViewController = storyboard.instantiateViewController(withIdentifier: "ConnectionViewController") as! ConnectionViewController
            //let connectViewController = ConnectionViewController( nibName: nil, bundle: nil )
            NSLog("Main view controller initialized: ... not sure")
            self.navigatinController = UINavigationController(rootViewController: connectViewController!)
            NSLog("navigation controller : ... not sure")
            window.rootViewController = self.navigatinController
            self.navigatinController?.delegate = self
            NSLog("Root view controller: ... not sure")
            window.makeKeyAndVisible()
        }
        
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
    
    func socket(_ sock: GCDAsyncSocket, didConnectToHost host: String, port: UInt16) {
        NSLog("Connected to on port: 5555")
        //let d :Data = Data("BonjourC'estmoi".bytes)
        let idStr = "\((self.connectViewController?.remoteSwitch.isOn)! ? "Remote" : "Player"):"+uuid+":\r\n"
        socket?.write( Data(idStr.utf8), withTimeout: -1, tag: 0)
        sock.readData(withTimeout: -1, tag: 0)
        connected=true
        
        
        if self.settingViewController != nil {
            self.settingViewController?.connectStatus.text = "Connected ? : YES"
            self.settingViewController?.connectStatus.setNeedsDisplay()
        }
        
        if self.remoteViewController != nil {
            self.remoteViewController?.connectStatus.text = "Connected ? : YES"
            self.remoteViewController?.connectStatus.setNeedsDisplay()
            self.socket?.write(Data("VideoList:\r\n".utf8), withTimeout: -1, tag: 0)
        }
        
    }
    
    func socketDidDisconnect(_ sock: GCDAsyncSocket, withError err: Error?) {
        // NSLog("Socket did disconnect from  %s %d", sock.connectedHost!, sock.localPort)
        NSLog("Socket did disconnect from  \(sock.connectedHost) \(sock.localPort)")
        connected = false
        
        if self.settingViewController != nil {
            self.settingViewController?.connectStatus.text = "Connected ? : NO"
            self.settingViewController?.connectStatus.setNeedsDisplay()
        }
        
        if self.remoteViewController != nil {
            self.remoteViewController?.connectStatus.text = "Connected ? : NO"
            self.remoteViewController?.connectStatus.setNeedsDisplay()
            self.socket?.write(Data("VideoList:\r\n".utf8), withTimeout: -1, tag: 0)
        }
        let timer = Timer.scheduledTimer(timeInterval:Date(timeIntervalSinceNow: 3).timeIntervalSinceNow, target: self, selector: #selector(self.reconnect), userInfo: nil, repeats: false)
        
        //let t = Timer.scheduledTimer(withTimeInterval: 4, repeats: false) { (t:Timer) in
        //self.socket = GCDAsyncSocket(self,)
        
        
    }
    func socketDidCloseReadStream(_ sock: GCDAsyncSocket) {
        
        self.connected = false
        //let timer = Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(self.reconnect), userInfo: nil, repeats: false)
        //let t = Timer.scheduledTimer(withTimeInterval: 4, repeats: false) { (t:Timer) in
        //self.socket = GCDAsyncSocket(self,)
    }
    
    func socket(_ sock: GCDAsyncSocket, didRead data: Data, withTag tag: Int) {
        //
        
        let str = String(data: data, encoding: .utf8) ?? ""
        let lines = str.split(separator: "\n")
        print("Receive Data: \(str) / lines: \(lines)")
        for line in lines {
            NSLog("line ====> \(line)")
            var parts = line.split(separator: ":")
            let cmd = parts[0]
            if parts.count > 0 {
                if cmd == "Prepare" {
                    NSLog("Prepare")
                    if self.navigatinController?.visibleViewController  == self.settingViewController && self.settingViewController != nil{
                        // Add instructions to create a new sto
                        let apdel = self
                        
                        let storyborad = UIStoryboard.init(name: "Main", bundle: nil)
                        let playerVC : PlayerViewController =  storyborad.instantiateViewController(withIdentifier:"PlayerViewController") as! PlayerViewController
                        playerVC.videoName = String(parts[1])
                        playerVC.delegate = apdel.settingViewController
                        apdel.playerViewController = playerVC
                        apdel.navigatinController?.pushViewController(playerVC, animated: true)
                        //apdel.playerViewController?.loadVideo(String(parts[1]))
                        
                    } else if  self.navigatinController?.visibleViewController == self.playerViewController {
                        let apdel = self
                        
                        self.playerViewController?.loadVideo(String(parts[1]))
                        //apdel.navigatinController?.pushViewController(playerVC, animated: true)
                        //apdel.playerViewController?.loadVideo(String(parts[1]))
                    }
                    
                } else if cmd == "Play" {
                    if self.navigatinController?.visibleViewController  == self.playerViewController && self.playerViewController != nil{
                        // Add instructions to create a new sto
                        let apdel = self
                        
                        apdel.playerViewController?.play()
                        
                    }
                } else if cmd == "Pause" {
                    if self.navigatinController?.visibleViewController  == self.playerViewController && self.playerViewController != nil{
                        // Add instructions to create a new sto
                        let apdel = self
                        //DispatchQueue.main.async {
                        // vérifier que la time stamp est bien passée
                        if (parts.count > 1) {
                            let s : String = String(parts[1])
                            let d : Double = (s as NSString).doubleValue
                            apdel.playerViewController?.pause(timestamp: d)
                        } else {
                            apdel.playerViewController?.pause()
                        }
                        //}
                    }
                } else if cmd == "Stop" {
                    
                    if self.navigatinController?.visibleViewController  == self.playerViewController && self.playerViewController != nil{
                        // Add instructions to create a new sto
                        let apdel = self
                        //DispatchQueue.main.async {
                        apdel.playerViewController?.stop() // remet la lecture à zéro
                        //}
                    }
                } else if cmd == "Videos" {
                    NSLog("\(parts.count) =======")
                    if self.settingViewController != nil{
                        // Add instructions to create a new sto
                        let apdel = self
                        NSLog("\(parts.count) ======= 3333")
                        
                        //let download = DownloadInfo("http://releases.ubuntu.com/18.10/ubuntu-18.10-desktop-amd64.iso",percentage: 0)
                        let download = DownloadInfo("http://\(apdel.address):8000/\(apdel.settingViewController?.selectedPosition ?? 0)/\(String(parts[1]))",percentage: 0)
                        NSLog("http://\(apdel.address)/\(apdel.settingViewController?.selectedPosition ?? 0)/\(String(parts[1]))")
                        //let download = DownloadInfo("http://\(apdel.address):8000/\(String(parts[1]))",percentage: 0)
                        apdel.settingViewController?.downloads.add(download)
                        
                        
                        download.startDownload(self.settingViewController!)
                        apdel.settingViewController?.tableView.reloadData()
                        // remet la lecture à zéro
                        //download.delegate = apdel.settingsViewController
                        
                    }
                } else if cmd == "VideoList" {
                    //let apdel = self
                    if self.remoteViewController != nil {
                        NSLog("\(line), VideoList")
                        videoList.removeAllObjects()
                        parts.removeFirst()
                        for p in parts {
                            let s = String(p)
                            videoList.add(s)
                        }
           
                        self.remoteViewController?.videoList = videoList
                        self.remoteViewController?.VideoList.reloadData()
                        
                    }
                    
                } else{
                    NSLog("Unknown command \(cmd)")
                }
            }
        }
        sock.readData(withTimeout: -1, tag: 0)
    }
    
    func socket(_ sock: GCDAsyncSocket, didWriteDataWithTag tag: Int) {
        //
        NSLog("Socket did write data")
        sock.readData(withTimeout: -1, tag: 0)
    }
    
    @objc func reconnect() {
        if (!connected) {
            NSLog("Trying to reconnect")
            let appDel = self
            appDel.socket = GCDAsyncSocket(delegate: appDel, delegateQueue: DispatchQueue.main)
            do {
                try appDel.socket?.connect(toHost: appDel.address , onPort: appDel.port)
                
            } catch {
                NSLog ("Error \(error)")
            }
        }
    }
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        NSLog("Navigation controller \(navigationController) did show \(viewController)")
        if viewController == connectViewController {
            self.remoteViewController = nil
            self.settingViewController = nil
            if connected {
                self.socket?.disconnect()
                self.socket?.delegate = nil
            }
        } else if viewController == settingViewController && settingViewController != nil{
            self.playerViewController = nil
        }
    }
    
    
    
}

